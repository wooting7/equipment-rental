package com.syw.equipmentrental.entity;

import com.syw.equipmentrental.enums.Department;
import com.syw.equipmentrental.intefaces.CommonModelBuilder;
import com.syw.equipmentrental.model.MemberJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    private Department department;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 20)
    private String memberPhone;

    @Column(nullable = false, length = 10)
    private String memberPosition;

    @Column(nullable = false)
    private Boolean isEnableMember;

    @Column
    private LocalDateTime dateWithdrawal;

    public void putWithdrawal() {
        this.isEnableMember = false;
        this.dateWithdrawal = LocalDateTime.now();
    }

    private Member(MemberBuilder builder) {
        this.department = builder.department;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.memberPosition = builder.memberPosition;
        this.isEnableMember = builder.isEnableMember;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final Department department;
        private final String memberName;
        private final String memberPhone;
        private final String memberPosition;
        private final Boolean isEnableMember;


        public MemberBuilder(MemberJoinRequest joinRequest) {
            this.department = joinRequest.getDepartment();
            this.memberName = joinRequest.getMemberName();
            this.memberPhone = joinRequest.getMemberPhone();
            this.memberPosition = joinRequest.getMemberPosition();
            this.isEnableMember = true;
        }
        @Override
        public Member build() {
            return new Member(this);
        }
        }
}
