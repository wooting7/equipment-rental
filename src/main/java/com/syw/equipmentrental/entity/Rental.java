package com.syw.equipmentrental.entity;

import com.syw.equipmentrental.intefaces.CommonModelBuilder;
import com.syw.equipmentrental.model.RentalRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Rental {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipmentId",  nullable = false)
    private Equipment equipment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId",  nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateReturnReservation;

    @Column(nullable = false)
    private LocalDateTime dateRent;

    @Column(nullable = false)
    private Boolean isEnableRental;

    @Column
    private LocalDateTime dateReturn;

    public void putDateReturn() {
        this.isEnableRental = false;
        this.dateReturn = LocalDateTime.now();
    }

    private Rental (RentalBuilder builder) {
        this.equipment = builder.equipment;
        this.member = builder.member;
        this.dateReturnReservation = builder.dateReturnReservation;
        this.dateRent = builder.dateRent;
        this.isEnableRental = builder.isEnableRental;
    }

    public static class RentalBuilder implements CommonModelBuilder<Rental> {
        private final Equipment equipment;
        private final Member member;
        private final LocalDateTime dateReturnReservation;
        private final LocalDateTime dateRent;
        private final Boolean isEnableRental;


        public RentalBuilder (Equipment equipment, Member member, RentalRequest request) {
            this.equipment = equipment;
            this.member = member;
            this.dateReturnReservation = request.getDateReturnReservation();
            this.dateRent = request.getDateRent();
            this.isEnableRental = true;

        }
        @Override
        public Rental build() {
            return new Rental(this);
        }
    }

}
