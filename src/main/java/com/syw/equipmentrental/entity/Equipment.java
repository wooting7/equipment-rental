package com.syw.equipmentrental.entity;


import com.syw.equipmentrental.intefaces.CommonModelBuilder;
import com.syw.equipmentrental.model.EquipmentRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String equipmentName;

    @Column(nullable = false, length = 30)
    private String equipmentSpec;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column
    private LocalDateTime dateDestruction;

    public void putDestruction() {
        this.isEnable = false;
        this.dateDestruction = LocalDateTime.now();
    }
    private Equipment (EquipmentBuilder builder) {
        this.equipmentName = builder.equipmentName;
        this.equipmentSpec = builder.equipmentSpec;
        this.isEnable = builder.isEnable;
    }
    public static class EquipmentBuilder implements CommonModelBuilder<Equipment> {
        private final String equipmentName;
        private final String equipmentSpec;
        private final Boolean isEnable;

        public EquipmentBuilder (EquipmentRequest request) {
            this.equipmentName = request.getEquipmentName();
            this.equipmentSpec = request.getEquipmentSpec();
            this.isEnable = true;
        }
        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }

}
