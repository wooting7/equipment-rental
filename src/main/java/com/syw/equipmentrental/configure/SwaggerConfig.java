package com.syw.equipmentrental.configure;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;

@Configuration
@EnableSwagger2 // 스웨거2를 가능하게 할거야
public class SwaggerConfig {
    private String version;
    private String title;
    private final String TITLE_FIX = "기자재 이용 관리 API ";
        // 상수값을 지정한거, 변하지 않음
    @Bean
    public Docket apiV1() {
        version = "V1";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.syw.equipmentrental"))
                // 스피링폭스에서 제공하는거 / 괄호안에 com~ 넣음
                .paths(PathSelectors.ant("/v1/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                // 여끼까지 바구니에 넣지 못하고 getApiInfo 메소드 만듬
                .securitySchemes(Collections.singletonList(getApikey()))
                .enable(true);
    }

    @Bean
    public Docket apiV2() {
        version = "V2";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.syw.equipmentrental"))
                // 스피링폭스에서 제공하는거 / 괄호안에 com~ 넣음
                .paths(PathSelectors.ant("/v2/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                // 여끼까지 바구니에 넣지 못하고 getApiInfo 메소드 만듬
                .securitySchemes(Collections.singletonList(getApikey()))
                .enable(true);
    }
    private ApiInfo getApiInfo(String title, String version) {
        return new ApiInfo(
                title,
                "Swagger API Docs",
                version,
                "wooting.com",
                new Contact("syw","wooting.com","wooting7@gmail.com"),
                "License",
                "wooting.com",
                new ArrayList<>()
        );
    }
    private ApiKey getApikey() {
        return new ApiKey("jwtToken", "X-AUTH-TOKEN", "header");
    }
}
