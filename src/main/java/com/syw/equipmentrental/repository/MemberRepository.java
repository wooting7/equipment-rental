package com.syw.equipmentrental.repository;

import com.syw.equipmentrental.entity.Member;
import com.syw.equipmentrental.model.MemberItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByIsEnableMemberOrderByIdDesc(Boolean isEnable);
}
