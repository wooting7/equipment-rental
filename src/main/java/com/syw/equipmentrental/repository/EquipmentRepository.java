package com.syw.equipmentrental.repository;

import com.syw.equipmentrental.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    List<Equipment> findAllByIsEnableOrderByIdDesc(Boolean isEnable);


}
