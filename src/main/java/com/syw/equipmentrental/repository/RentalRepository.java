package com.syw.equipmentrental.repository;

import com.syw.equipmentrental.entity.Rental;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface RentalRepository extends JpaRepository<Rental, Long> {

    List<Rental> findAllByDateRentGreaterThanEqualAndDateRentLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);




}
