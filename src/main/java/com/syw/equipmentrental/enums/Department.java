package com.syw.equipmentrental.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Department {
    HUMAN_RESOURCES_TEAM("인사팀"),
    PLANNING_TEAM("기획팀"),
    RESEARCH_DEVELOP_TEAM("연구개발팀"),
    SALES_TEAM("영업팀"),
    CUSTOMER_SATISFACTION_TEAM("고객만족팀"),
    OFFICE_OF_THE_SECRETARY("비서실");

    private final String name;
}
