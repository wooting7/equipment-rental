package com.syw.equipmentrental.exception;

public class CNoRentalDataException extends RuntimeException {
    public CNoRentalDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoRentalDataException(String msg) {
        super(msg);
    }

    public CNoRentalDataException() {
        super();
    }
}
