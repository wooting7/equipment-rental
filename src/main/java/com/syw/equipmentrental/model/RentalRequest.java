package com.syw.equipmentrental.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class RentalRequest {

    @NotNull
    @ApiModelProperty(notes = "기자재번호",required = true)
    private Long equipmentId;

    @NotNull
    @ApiModelProperty(notes = "사원번호",required = true)
    private Long memberId;

    @NotNull
    @ApiModelProperty(notes = "반납예정일",required = true)
    private LocalDateTime dateReturnReservation;

    @NotNull
    @ApiModelProperty(notes = "대여일", required = true)
    private LocalDateTime dateRent;
}
