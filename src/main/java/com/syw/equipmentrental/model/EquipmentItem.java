package com.syw.equipmentrental.model;

import com.syw.equipmentrental.entity.Equipment;
import com.syw.equipmentrental.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {

    @ApiModelProperty(notes = "기자재번호",required = true)
    private Long id;

    @ApiModelProperty(notes = "기자재이름",required = true)
    private String equipmentName;

    @ApiModelProperty(notes = "기자재스펙",required = true)
    private String equipmentSpec;

    @ApiModelProperty(notes = "기자재상태",required = true)
    private String isEnable;

    @ApiModelProperty(notes = "폐기일자")
    private LocalDateTime dateDestruction;

    private EquipmentItem (EquipmentItemBuilder builder) {
        this.id = builder.id;
        this.equipmentName = builder.equipmentName;
        this.equipmentSpec = builder.equipmentSpec;
        this.isEnable = builder.isEnable;
        this.dateDestruction = builder.dateDestruction;
    }

    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem> {
        private final Long id;
        private final String equipmentName;
        private final String equipmentSpec;
        private final String isEnable;
        private final LocalDateTime dateDestruction;

        public EquipmentItemBuilder (Equipment equipment) {
            this.id = equipment.getId();
            this.equipmentName = equipment.getEquipmentName();
            this.equipmentSpec = equipment.getEquipmentSpec();
            this.isEnable = equipment.getIsEnable() ? "양품" : "폐기";
            this.dateDestruction = equipment.getDateDestruction();

        }
        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }



}
