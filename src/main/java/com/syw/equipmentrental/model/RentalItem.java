package com.syw.equipmentrental.model;

import com.syw.equipmentrental.entity.Rental;
import com.syw.equipmentrental.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RentalItem {

    @ApiModelProperty(notes = "대여번호",required = true)
    private Long rentalId;

    @ApiModelProperty(notes = "반납예정일",required = true)
    private LocalDateTime dateReturnReservation;

    @ApiModelProperty(notes = "대여일", required = true)
    private LocalDateTime dateRent;

    @ApiModelProperty(notes = "대여상태", required = true)
    private String  isEnableRental;

    @ApiModelProperty(notes = "반납일")
    private LocalDateTime dateReturn;



    @ApiModelProperty(notes = "기자재번호",required = true)
    private Long equipmentId;

    @ApiModelProperty(notes = "기자재이름",required = true)
    private String equipmentName;



    @ApiModelProperty(notes = "사원번호",required = true)
    private Long memberId;

    @ApiModelProperty(notes = "부서+사원이름",required = true)
    private String departmentMemberName;

    @ApiModelProperty(notes = "사원전화번호",required = true)
    private String memberPhone;

    @ApiModelProperty(notes = "사원직책",required = true)
    private String memberPosition;

    @ApiModelProperty(notes = "사원상태",required = true)
    private String  isEnableMember;

    @ApiModelProperty(notes = "사원퇴사일")
    private LocalDateTime dateWithdrawal;

    private RentalItem (RentalItemBuilder builder) {
        this.rentalId = builder.rentalId;
        this.dateReturnReservation = builder.dateReturnReservation;
        this.dateRent = builder.dateRent;
        this.isEnableRental = builder.isEnableRental;
        this.dateReturn = builder.dateReturn;

        this.equipmentId = builder.equipmentId;
        this.equipmentName = builder.equipmentName;

        this.memberId = builder.memberId;
        this.departmentMemberName = builder.departmentMemberName;
        this.memberPhone = builder.memberPhone;
        this.memberPosition = builder.memberPosition;
        this.isEnableMember = builder.isEnableMember;
        this.dateWithdrawal = builder.dateWithdrawal;

    }


    public static class RentalItemBuilder implements CommonModelBuilder<RentalItem> {
        private final Long rentalId;
        private final LocalDateTime dateReturnReservation;
        private final LocalDateTime dateRent;
        private final String  isEnableRental;
        private final LocalDateTime dateReturn;

        private final Long equipmentId;
        private final String equipmentName;

        private final Long memberId;
        private final String departmentMemberName;
        private final String memberPhone;
        private final String memberPosition;
        private final String  isEnableMember;
        private final LocalDateTime dateWithdrawal;

        public RentalItemBuilder (Rental rental) {
            this.rentalId = rental.getId();
            this.dateReturnReservation = rental.getDateReturnReservation();
            this.dateRent = LocalDateTime.now();
            this.isEnableRental = rental.getIsEnableRental() ? "대여중" : "반납완료";
            this.dateReturn = rental.getDateReturn();

            this.equipmentId = rental.getEquipment().getId();
            this.equipmentName = rental.getEquipment().getEquipmentName();

            this.memberId = rental.getMember().getId();
            this.departmentMemberName = rental.getMember().getDepartment() + " " + rental.getMember().getMemberName();
            this.memberPhone = rental.getMember().getMemberPhone();
            this.memberPosition = rental.getMember().getMemberPhone();
            this.isEnableMember = rental.getMember().getIsEnableMember() ? "근무중" : "퇴사";
            this.dateWithdrawal = rental.getMember().getDateWithdrawal();
        }

        @Override
        public RentalItem build() {
            return new RentalItem(this);
        }
    }
}
