package com.syw.equipmentrental.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EquipmentRequest {

    @ApiModelProperty(notes = "기자재이름",required = true)
    @NotNull
    @Length(min = 1, max = 30)
    private String equipmentName;

    @ApiModelProperty(notes = "기자재스펙",required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String equipmentSpec;
}
