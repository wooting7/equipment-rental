package com.syw.equipmentrental.model;

import com.syw.equipmentrental.enums.Department;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class MemberJoinRequest {
    @ApiModelProperty(notes = "부서",required = true)
    private Department department;

    @ApiModelProperty(notes = "사원이름",required = true)
    @NotNull @Length(min = 2, max = 20)
    private String memberName;

    @NotNull
    @ApiModelProperty(notes = "사원전화번호",required = true)
    private String memberPhone;

    @NotNull
    @ApiModelProperty(notes = "사원직책",required = true)
    private String memberPosition;
}
