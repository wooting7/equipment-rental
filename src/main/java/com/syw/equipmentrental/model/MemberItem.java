package com.syw.equipmentrental.model;

import com.syw.equipmentrental.entity.Member;
import com.syw.equipmentrental.enums.Department;
import com.syw.equipmentrental.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "사원번호",required = true)
    private Long id;

    @ApiModelProperty(notes = "부서",required = true)
    private Department department;

    @ApiModelProperty(notes = "사원이름",required = true)
    private String memberName;

    @ApiModelProperty(notes = "사원전화번호",required = true)
    private String memberPhone;

    @ApiModelProperty(notes = "사원직책",required = true)
    private String memberPosition;

    @ApiModelProperty(notes = "사원상태",required = true)
    private String  isEnable;

    @ApiModelProperty(notes = "사원퇴사일")
    private LocalDateTime dateWithdrawal;

    private MemberItem (MemberItemBuilder builder) {
        this.id = builder.id;
        this.department = builder.department;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.isEnable = builder.isEnableMember;
        this.dateWithdrawal = builder.dateWithdrawal;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final Department department;
        private final String memberName;
        private final String memberPhone;
        private final String  isEnableMember;
        private final LocalDateTime dateWithdrawal;

        public MemberItemBuilder (Member member) {
            this.id = member.getId();
            this.department = member.getDepartment();
            this.memberName = member.getMemberName();
            this.memberPhone = member.getMemberPhone();
            this.isEnableMember = member.getIsEnableMember() ? "근무중" : "퇴사";
            this.dateWithdrawal = member.getDateWithdrawal();
        }
        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
