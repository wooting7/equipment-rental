package com.syw.equipmentrental.controller;


import com.syw.equipmentrental.entity.Equipment;
import com.syw.equipmentrental.entity.Member;
import com.syw.equipmentrental.model.CommonResult;
import com.syw.equipmentrental.model.ListResult;
import com.syw.equipmentrental.model.RentalItem;
import com.syw.equipmentrental.model.RentalRequest;
import com.syw.equipmentrental.service.EquipmentService;
import com.syw.equipmentrental.service.MemberService;
import com.syw.equipmentrental.service.RentalService;
import com.syw.equipmentrental.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;


@Api(tags = "기자재대여내역 등록")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/rental")
public class RentalController {
    private final MemberService memberService;
    private final EquipmentService equipmentService;
    private final RentalService rentalService;

    @ApiOperation(value = "대여내역 등록하기")
    @PostMapping("/new")
    public CommonResult setRental(@RequestBody RentalRequest request) {
        Member member = memberService.getMemberDate(request.getMemberId());
        Equipment equipment = equipmentService.getEquipmentDate(request.getEquipmentId());
        rentalService.setRental(member, equipment, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "대여내역 확인하기")
    @GetMapping("/serch")
    public ListResult<RentalItem> getRentals(
            @RequestParam(value = "dataStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate dateEnd
    ) { return ResponseService.getListResult(rentalService.getRentals(dateStart, dateEnd), true);
    }


    @ApiOperation(value = "기자재 반납하기")
    @PutMapping("/{id}")
    public CommonResult putRental(@PathVariable long id) {
        rentalService.putDateReturn(id);
        return ResponseService.getSuccessResult();
    }

}
