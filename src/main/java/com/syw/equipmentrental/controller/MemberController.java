package com.syw.equipmentrental.controller;

import com.syw.equipmentrental.model.CommonResult;
import com.syw.equipmentrental.model.ListResult;
import com.syw.equipmentrental.model.MemberItem;
import com.syw.equipmentrental.model.MemberJoinRequest;
import com.syw.equipmentrental.service.MemberService;
import com.syw.equipmentrental.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원 등록")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "사원정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원정보 조회하기")
    @GetMapping("/search")
    public ListResult<MemberItem> getMembers(@RequestParam(required = false) Boolean isEnableMember) {
        if (isEnableMember == null ) {
            return ResponseService.getListResult(memberService.getMembers(),true);
        } else {
            return ResponseService.getListResult(memberService.getMembers(isEnableMember), true);
        }
    }

    @ApiOperation(value = "사원정보 삭제하기")
    @DeleteMapping("/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);
        return ResponseService.getSuccessResult();
    }
}