package com.syw.equipmentrental.controller;

import com.syw.equipmentrental.model.CommonResult;
import com.syw.equipmentrental.model.EquipmentItem;
import com.syw.equipmentrental.model.EquipmentRequest;
import com.syw.equipmentrental.model.ListResult;
import com.syw.equipmentrental.service.EquipmentService;
import com.syw.equipmentrental.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Api(tags = "기자재 등록")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/equipment")
public class EquipmentController {
    private final EquipmentService equipmentService;

    @ApiOperation(value = "기자재정보 등록하기")
    @PostMapping("/new")
    public CommonResult setEquipment(@RequestBody @Valid EquipmentRequest request) {
        equipmentService.setEquipment(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재정보 조회하기")
    @GetMapping("/search")
    public ListResult<EquipmentItem> getEquipments(@RequestParam(required = false) Boolean isEnable) {
        if (isEnable == null) {
            return ResponseService.getListResult(equipmentService.getEquipments(),true);
        } else {
            return ResponseService.getListResult(equipmentService.getEquipments(isEnable), true);
        }
    }

    @ApiOperation(value = "기자재정보 수정하기")
    @PutMapping("/{id}")
    public CommonResult putEquipment(@PathVariable long id) {
        equipmentService.putEquipment(id);
        return ResponseService.getSuccessResult();
    }
}
