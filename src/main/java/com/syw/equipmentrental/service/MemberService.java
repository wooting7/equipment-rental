package com.syw.equipmentrental.service;

import com.syw.equipmentrental.entity.Member;
import com.syw.equipmentrental.exception.CMissingDataException;
import com.syw.equipmentrental.exception.CNoMemberDataException;
import com.syw.equipmentrental.model.ListResult;
import com.syw.equipmentrental.model.MemberItem;
import com.syw.equipmentrental.model.MemberJoinRequest;
import com.syw.equipmentrental.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 회원 시퀀스 가져오는 메서드
     * @param id
     * @return 회원 시퀀스
     */
    public Member getMemberDate(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     *  회원 등록
     * @param joinRequest 기본정보
     */
    public void setMember(MemberJoinRequest joinRequest) {
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }

    /**
     * 회원 정보 조회
     * @return 회원 정보
     */
    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 회원 정보 조회
     * @param isEnableMember 재직중, 퇴사
     * @return 회원 정보
     */
    public ListResult<MemberItem> getMembers(Boolean isEnableMember) {
        List<Member> members = memberRepository.findAllByIsEnableMemberOrderByIdDesc(isEnableMember);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    //퇴사 생성

    /**
     * 회원정보 탈퇴
     * @param id
     */
    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        if (!member.getIsEnableMember()) throw new CNoMemberDataException();
        member.putWithdrawal();
        memberRepository.save(member);
    }
}
