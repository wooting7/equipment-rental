package com.syw.equipmentrental.service;

import com.syw.equipmentrental.entity.Equipment;
import com.syw.equipmentrental.exception.CMissingDataException;
import com.syw.equipmentrental.exception.CNoMemberDataException;
import com.syw.equipmentrental.model.EquipmentItem;
import com.syw.equipmentrental.model.EquipmentRequest;
import com.syw.equipmentrental.model.ListResult;
import com.syw.equipmentrental.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {

    private final EquipmentRepository equipmentRepository;

    /**
     * 기자재 정보 가져오는 메서드
     * @param id 기자재 시퀀스
     * @return 기자재 정보
     */
    public Equipment getEquipmentDate(long id) {
        return equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 기자재 정보 등록
     * @param request 기자재 기본정보
     */
    public void setEquipment(EquipmentRequest request) {
        Equipment equipment = new Equipment.EquipmentBuilder(request).build();
        equipmentRepository.save(equipment);
    }

    /**
     * 기자재 정보 조회
     * @return 기자재 정보
     */
    public ListResult<EquipmentItem> getEquipments() {
        List<Equipment> equipments = equipmentRepository.findAll();

        List<EquipmentItem> result = new LinkedList<>();
        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 기자재 정보 조회
     * @param isEnable 사용가능 사용불가능(폐기)
     * @return 기자재정보
     */
    public ListResult<EquipmentItem> getEquipments(Boolean isEnable) {
        List<Equipment> equipments = equipmentRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<EquipmentItem> result = new LinkedList<>();
        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 기자재 사용불가능
     * @param id 기자재 시퀀스
     */
    public void putEquipment(long id) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        if (!equipment.getIsEnable()) throw new CMissingDataException();
        equipment.putDestruction();
        equipmentRepository.save(equipment);
    }
}
