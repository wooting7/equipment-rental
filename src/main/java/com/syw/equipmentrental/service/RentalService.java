package com.syw.equipmentrental.service;

import com.syw.equipmentrental.entity.Equipment;
import com.syw.equipmentrental.entity.Member;
import com.syw.equipmentrental.entity.Rental;
import com.syw.equipmentrental.exception.CMissingDataException;
import com.syw.equipmentrental.exception.CNoRentalDataException;
import com.syw.equipmentrental.model.ListResult;
import com.syw.equipmentrental.model.RentalItem;
import com.syw.equipmentrental.model.RentalRequest;
import com.syw.equipmentrental.repository.RentalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RentalService {
    private final RentalRepository rentalRepository;

    /**
     * 대여 등록
     * @param member 회원정보
     * @param equipment 기자재정보
     * @param request 대여이력
     */
    public void setRental(Member member, Equipment equipment, RentalRequest request) {
        Rental rental = new Rental.RentalBuilder(equipment, member, request).build();
        rentalRepository.save(rental);
    }

    /**
     * 대여내역 조회
     * @param dateStart 시작일
     * @param dateEnd 종료일
     * @return
     */
    public ListResult<RentalItem> getRentals(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dataStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0, 0, 0
        );
        LocalDateTime dataEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23, 59, 59
        );

        List<Rental> rentals = rentalRepository.findAllByDateRentGreaterThanEqualAndDateRentLessThanEqualOrderByIdDesc(dataStartTime, dataEndTime);

        List<RentalItem> result = new LinkedList<>();
        rentals.forEach(rental -> {
            RentalItem addItem = new RentalItem.RentalItemBuilder(rental).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 기자재 반납
     * @param id 대여내역 시퀀스
     */
    public void putDateReturn (long id) {
        Rental rental = rentalRepository.findById(id).orElseThrow(CMissingDataException::new);
        if (!rental.getIsEnableRental()) throw new CNoRentalDataException();
        rental.putDateReturn();
        rentalRepository.save(rental);
    }
}
